package event_editor;

/**
 * Modified EventListener that can be notified by string events.
 * 
 * These are used extensively to communicate status from one dialog to another dialog boxes.
 * For example, the EventTagDialog can know if EventTagDialog was saved or cancelled.
 * 
 * @author Bibek Shrestha <bibek.shrestha@tum.de>
 */
abstract public class EventListener implements java.util.EventListener
{
    /**
     * Notify the listener through string event types
     * 
     * @param eventType Name of the event as a String
     */
    abstract public void notify(String eventType);
}
