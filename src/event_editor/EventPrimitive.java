package event_editor;

import java.util.HashMap;
import java.util.Map;

/**
 * An event Primitive acts as a container for EventEntity. 
 * 
 * EventPrimitive represents Event related OSM Tags for given Primitive (Node, Way, Relation) into.
 * It contains an EventMap, which is a collection of EventEntity Objects that represents one or 
 * more events associated with a given OSM Primitive.
 * 
 * @author Bibek Shrestha <bibek.shrestha@tum.de>
 *
 */
public class EventPrimitive
{

	protected Map<Integer, EventEntity> eventMap = new HashMap<Integer, EventEntity>();
	protected boolean isEvent = true;
	protected Integer highestEventNumber = -1;

	public boolean isEvent()
	{
		return isEvent;
	}

	public void setIsEvent(boolean isEvent)
	{
		this.isEvent = isEvent;
	}

	/**
	 * @return HashMap of list of all EventEntity for this OSM Primitive.
	 */
	public Map<Integer, EventEntity> getEventMap()
	{
		return this.eventMap;
	}

	@Override
	public String toString()
	{
		return "EventPrimitive isEvent:" + isEvent + " , "
		        + eventMap.toString();
	}

	public Integer getNextHighestEventNumber()
	{
		return highestEventNumber + 1;
	}

	public void setHighestEventNumber(Integer i)
	{
		this.highestEventNumber = i;
	}
}
